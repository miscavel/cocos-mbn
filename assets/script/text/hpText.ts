
import { _decorator, Component, Node, RichText } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('HpText')
export class HpText extends Component {
    private richText?: RichText | null;

    onLoad() {
        this.richText = this.getComponent(RichText);
    }

    private getHpText(hp: number) {
        return `<outline color=#404961 width=2><b>${Math.round(hp)}</b></outline>`;
    }

    public setHpText(hp: number) {
        const { richText } = this;
        if (richText) {
            richText.string = this.getHpText(hp);
        }
    }
}