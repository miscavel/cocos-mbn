
import { tween, Tween, UIOpacity, _decorator } from 'cc';
import { ASSET_KEY } from '../enum/asset';
import { BaseSprite } from './baseSprite';
const { ccclass, property } = _decorator;

@ccclass('PressStart')
export class PressStart extends BaseSprite {
    private fadeTween?: Tween<UIOpacity>;

    constructor() {
        super('PressStart', ASSET_KEY.TITLE_PRESS_START);
    }

    show() {
        const { uiOpacity } = this;

        if (!uiOpacity) return;

        this.setOpacity(0);
        this.fadeTween = tween(uiOpacity)
            .repeatForever(
                tween()
                .to(
                    0.75, 
                    { opacity: 255 },
                )
                .to(
                    0.75, 
                    { opacity: 0 },
                )
            )
            .start();
    }

    hide() {
        this.fadeTween?.stop();
        this.setOpacity(0);
    }
}