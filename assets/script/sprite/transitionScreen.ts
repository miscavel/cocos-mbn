
import { _decorator, tween, Color } from 'cc';
import { ASSET_KEY } from '../enum/asset';
import { TRANSITION_SCREEN_EVENT } from '../enum/transitionScreen';
import { BaseSprite } from './baseSprite';
const { ccclass, property } = _decorator;

@ccclass('TransitionScreen')
export class TransitionScreen extends BaseSprite {
    constructor() {
        super('TransitionScreen', ASSET_KEY.TRANSITION_SCREEN);
    }

    fadeIn(duration = 1, color = Color.BLACK) {
        this.reload();
        this.setColor(color);
        this.setOpacity(0);
        tween(this.uiOpacity).to(
            duration, 
            { opacity: 255 },
            {
                onStart: () => {
                    this.node.emit(TRANSITION_SCREEN_EVENT.FADE_IN_START);
                },
                onComplete: () => {
                    this.node.emit(TRANSITION_SCREEN_EVENT.FADE_IN_COMPLETE);
                }
            }
        ).start();
    }

    fadeOut(duration = 1, color = Color.BLACK) {
        this.reload();
        this.setColor(color);
        this.setOpacity(255);
        tween(this.uiOpacity).to(
            duration,
            { opacity: 0 },
            {
                onStart: () => {
                    this.node.emit(TRANSITION_SCREEN_EVENT.FADE_OUT_START);
                },
                onComplete: () => {
                    this.node.emit(TRANSITION_SCREEN_EVENT.FADE_OUT_COMPLETE);
                }
            }
        ).start();
    }
}