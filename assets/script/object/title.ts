
import { _decorator, Component, Node } from 'cc';
import { TitleSoundtrack } from '../audio/soundtrack/titleSoundtrack';
import { TitleBackground } from '../sprite/titleBackground';
const { ccclass, property } = _decorator;

@ccclass('Title')
export class Title extends Component {
    @property(TitleBackground)
    public readonly titleBackground?: TitleBackground;

    @property(TitleSoundtrack)
    public readonly titleSoundtrack?: TitleSoundtrack;

    public play() {
        this.titleBackground?.playAnimation();
        this.playSoundtrack();
    }

    public playSoundtrack() {
        this.titleSoundtrack?.play();
    }

    public stopSoundtrack() {
        this.titleSoundtrack?.stop();
    }

    public fadeOut(duration: number) {
        this.titleSoundtrack?.fadeOut(duration);
    }
}