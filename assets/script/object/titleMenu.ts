
import { _decorator, Component, Node } from 'cc';
import { CustomClick } from '../audio/sfx/customClick';
import { NewGameClick } from '../audio/sfx/newGameClick';
import { TITLE_MENU_STATE } from '../enum/titleMenu';
import { PressStart } from '../sprite/pressStart';
import { NewGameMenu } from './newGameMenu';
const { ccclass, property } = _decorator;

@ccclass('TitleMenu')
export class TitleMenu extends Component {
    private readonly stateProgress = [ 
        TITLE_MENU_STATE.INIT,
        TITLE_MENU_STATE.PRESS_START,
        TITLE_MENU_STATE.NEW_GAME_MENU,
        TITLE_MENU_STATE.ENTERING_GAME,
    ];

    @property(PressStart)
    public readonly pressStart?: PressStart;

    @property(NewGameMenu)
    public readonly newGameMenu?: NewGameMenu;

    @property(CustomClick)
    public readonly customClick?: CustomClick;

    @property(NewGameClick)
    public readonly newGameClick?: NewGameClick;

    private state: TITLE_MENU_STATE = TITLE_MENU_STATE.INIT;

    start() {
        this.hideAll();
    }
    
    private hideAll() {
        this.pressStart?.hide();
        this.newGameMenu?.hide();
    }

    private setState(state: TITLE_MENU_STATE) {
        const { PRESS_START, NEW_GAME_MENU } = TITLE_MENU_STATE;
        
        this.hideAll();

        /**
         * Prev state complete handle
         */
        switch(this.state) {
            case PRESS_START: {
                this.customClick?.play();
                break;
            }

            case NEW_GAME_MENU: {
                this.newGameClick?.play();
                break;
            }

            default: {
                break;
            }
        }

        /**
         * Next state handle
         */
        switch(state) {
            case PRESS_START: {
                this.pressStart?.show();
                break;
            }

            case NEW_GAME_MENU: {
                this.newGameMenu?.show();
                break;
            }

            default: {
                break;
            }
        }
        this.state = state;
    }

    private getNextState() {
        const { stateProgress, state } = this;
        return stateProgress[stateProgress.indexOf(state) + 1] ?? state;
    }

    public progress() {
        this.setState(this.getNextState());
    }

    public isEnteringGame() {
        return this.state === TITLE_MENU_STATE.ENTERING_GAME;
    }
}
