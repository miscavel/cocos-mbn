import { ASSET_EXTENSION, ASSET_KEY, ASSET_TYPE } from "../enum/asset";
import { AssetConfig } from "../interface/asset";

function getShopeeAssetUrl(url: string) {
    return `https://cf.shopee.co.id/file/${url}`;
}

export function getAssets() {
    const assets = new Array<AssetConfig>();

    assets.push({
        key: ASSET_KEY.DUMMY_1,
        type: ASSET_TYPE.IMAGE,
        url: getShopeeAssetUrl('6119dca1932fa645d831b3ab57614677'),
        ext: ASSET_EXTENSION.PNG,
    });

    // title
    assets.push({
        key: ASSET_KEY.TITLE_BACKGROUND_IMAGE,
        type: ASSET_TYPE.SPRITESHEET,
        url: '',
        localUrl: 'image/title',
        config: {
            frameWidth: 960,
            frameHeight: 640,
        }
    });
    assets.push({
        key: ASSET_KEY.TITLE_PRESS_START,
        type: ASSET_TYPE.IMAGE,
        url: '',
        localUrl: 'image/press_start',
    });
    assets.push({
        key: ASSET_KEY.TITLE_NEW_GAME,
        type: ASSET_TYPE.IMAGE,
        url: '',
        localUrl: 'image/new_game',
    });

    // general UI
    assets.push({
        key: ASSET_KEY.TRANSITION_SCREEN,
        type: ASSET_TYPE.IMAGE,
        url: '',
        localUrl: 'image/transitionScreen',
    });

    // menu UI
    assets.push({
        key: ASSET_KEY.MENU_ARROW,
        type: ASSET_TYPE.IMAGE,
        url: '',
        localUrl: 'image/menu_arrow',
    });
    
    // game UI
    assets.push({
        key: ASSET_KEY.JOYPAD,
        type: ASSET_TYPE.IMAGE,
        url: '',
        localUrl: 'image/gamepad',
    });

    // battle
    assets.push({
        key: ASSET_KEY.FULL_PANEL,
        type: ASSET_TYPE.IMAGE,
        url: '',
        localUrl: 'image/full_panel',
    });
    assets.push({
        key: ASSET_KEY.BATTLE_DECO_MEGAMAN,
        type: ASSET_TYPE.SPRITESHEET,
        url: '',
        localUrl: 'image/battle_deco_megaman',
        config: {
            frameWidth: 32,
            frameHeight: 32,
            paddingX: 1,
        }
    });
    assets.push({
        key: ASSET_KEY.MEGAMAN_BATTLE,
        type: ASSET_TYPE.SPRITESHEET,
        url: '',
        localUrl: 'image/megaman_battle',
        config: {
            frameWidth: 66,
            frameHeight: 54,
            paddingY: 1,
        }
    });
    assets.push({
        key: ASSET_KEY.CANNON_BATTLE,
        type: ASSET_TYPE.SPRITESHEET,
        url: '',
        localUrl: 'image/cannon_battle',
        config: {
            frameWidth: 61,
            frameHeight: 55,
            paddingX: 1,
        }
    });
    assets.push({
        key: ASSET_KEY.HIT_EXPLOSION,
        type: ASSET_TYPE.SPRITESHEET,
        url: '',
        localUrl: 'image/hit_explosion',
        config: {
            frameWidth: 31,
            frameHeight: 46,
            paddingX: 1,
        }
    });

    // Soundtrack
    assets.push({
        key: ASSET_KEY.TITLE_BACKGROUND_SOUNDTRACK,
        type: ASSET_TYPE.AUDIO,
        url: '',
        localUrl: 'audio/soundtrack/title',
    });
    assets.push({
        key: ASSET_KEY.BATTLEFIELD_SOUNTRACK,
        type: ASSET_TYPE.AUDIO,
        url: '',
        localUrl: 'audio/soundtrack/battlefield',
    });
    assets.push({
        key: ASSET_KEY.BATTLE_VICTORY_SHORT,
        type: ASSET_TYPE.AUDIO,
        url: '',
        localUrl: 'audio/soundtrack/victory_short',
    });
    assets.push({
        key: ASSET_KEY.BATTLE_GAME_OVER,
        type: ASSET_TYPE.AUDIO,
        url: '',
        localUrl: 'audio/soundtrack/game_over',
    });

    // Misc track
    assets.push({
        key: ASSET_KEY.SILENT_TRACK,
        type: ASSET_TYPE.AUDIO,
        url: '',
        localUrl: 'audio/misc/silence',
    });

    // SFX
    assets.push({
        key: ASSET_KEY.CUSTOM_CLICK_SFX,
        type: ASSET_TYPE.AUDIO,
        url: '',
        localUrl: 'audio/sfx/custom_click',
    });
    assets.push({
        key: ASSET_KEY.NEW_GAME_CLICK_SFX,
        type: ASSET_TYPE.AUDIO,
        url: '',
        localUrl: 'audio/sfx/new_game',
    });

    // Battle SFX
    assets.push({
        key: ASSET_KEY.CANNON_SHOOT_SFX,
        type: ASSET_TYPE.AUDIO,
        url: '',
        localUrl: 'audio/sfx/cannon_shoot',
    });
    assets.push({
        key: ASSET_KEY.EXPLOSION_IMPACT_SFX,
        type: ASSET_TYPE.AUDIO,
        url: '',
        localUrl: 'audio/sfx/explosion_impact',
    });

    return assets;
}