import { _decorator, Component, Node, UIOpacity } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('JoypadControl')
export class JoypadControl extends Component {
    @property(Node)
    public up?: Node;

    @property(Node)
    public right?: Node;

    @property(Node)
    public down?: Node;

    @property(Node)
    public left?: Node;

    @property(Node)
    public zButton?: Node;

    start () {
        if (!this.isTablet() && !this.isMobile()) {
            this.node.active = false;
        }

        this.up?.on(Node.EventType.TOUCH_END, () => {
            this.node.emit('move', 0, -1);
        });

        this.right?.on(Node.EventType.TOUCH_END, () => {
            this.node.emit('move', 1, 0);
        });

        this.down?.on(Node.EventType.TOUCH_END, () => {
            this.node.emit('move', 0, 1);
        });

        this.left?.on(Node.EventType.TOUCH_END, () => {
            this.node.emit('move', -1, 0);
        });

        this.zButton?.on(Node.EventType.TOUCH_END, () => {
            this.node.emit('shoot');
        });
    }

    /**
     * Is not mobile return false
     */
    private isMobile() {
        return navigator.userAgent.indexOf('Mobile') !== -1;
    };
  
    /**
     * Is not tablet return false
     */
    private isTablet() {
        return navigator.userAgent.indexOf('Tablet') !== -1;
    };
}