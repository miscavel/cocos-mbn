export enum BATTLE_TILE_OWNER {
  PLAYER = 'player',
  ENEMY = 'enemy',
}