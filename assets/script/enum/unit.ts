export enum UNIT_STATE {
  READY = 'ready',
  MOVING = 'moving',
  ATTACKING = 'attacking',
  HURTING = 'hurting',
  DELETED = 'deleted',
}

export enum UNIT_EVENT {
  DELETED = 'deleted',
}