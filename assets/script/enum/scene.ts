export enum SCENE_KEY {
  PRELOAD = 'preload',
  TITLE = 'title',
  TRANSMISSION = 'transmission',
  BATTLE = 'battle',
}