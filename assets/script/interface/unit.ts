import { UNIT_STATE } from "../enum/unit";

export interface Unit {
  readonly afterMoveDelay: number;
  readonly afterAttackDelay: number;
  readonly afterHurtDelay: number;
  readonly invincibleDuration: number;
  maxHp: number;
  hp: number;
  state: UNIT_STATE;
  isInvincible: boolean;
  move(): boolean;
  attack(): boolean;
  getHurt(): boolean;
  getDeleted(): boolean;
  isReady(): boolean;
  turnInvincible(): boolean;
  takeDamage(damage: number): boolean;
}