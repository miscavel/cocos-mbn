import { _decorator } from 'cc';
import { ASSET_KEY } from '../../enum/asset';
import { BaseAudio } from '../baseAudio';
const { ccclass, property } = _decorator;

@ccclass('TitleSoundtrack')
export class TitleSoundtrack extends BaseAudio {
    constructor() {
        super('TitleSoundTrack', ASSET_KEY.TITLE_BACKGROUND_SOUNDTRACK, true, 0.4);
    }
}
