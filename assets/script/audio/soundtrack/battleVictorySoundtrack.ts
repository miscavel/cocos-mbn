import { _decorator, Component, Node } from 'cc';
import { ASSET_KEY } from '../../enum/asset';
import { BaseAudio } from '../baseAudio';
const { ccclass, property } = _decorator;

@ccclass('BattleVictorySoundtrack')
export class BattleVictorySoundtrack extends BaseAudio {
    constructor() {
        super('BattlefieldSoundtrack', ASSET_KEY.BATTLE_VICTORY_SHORT, false, 0.4);
    }
}
