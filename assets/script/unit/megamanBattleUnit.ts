import { tween, UIOpacity, _decorator } from 'cc';
import { MegamanBattle } from '../sprite/megamanBattle';
import { BattleUnit } from './battleUnit';
const { ccclass, property } = _decorator;

@ccclass('MegamanBattleUnit')
export class MegamanBattleUnit extends BattleUnit {
    @property(MegamanBattle)
    public readonly megamanBattle?: MegamanBattle;

    public move() {
        if (super.move()) {
            this.megamanBattle?.playFadeInAnimation();
            return true;
        }
        return false;
    }

    protected playBlinkInvincible() {
        const { invincibleDuration } = this;
        const uiOpacity = this.megamanBattle?.getComponent(UIOpacity);

        if (!uiOpacity) return;

        tween(uiOpacity).to(
            invincibleDuration,
            {},
            {
                onUpdate() {
                    if (uiOpacity.opacity === 200) {
                        uiOpacity.opacity = 60;
                    } else {
                        uiOpacity.opacity = 200;
                    }
                },
                onComplete() {
                    uiOpacity.opacity = 255;
                }
            }
        ).start();
    }

    public attack() {
        if (super.attack()) {
            this.megamanBattle?.playShootCannonAnimation();
            return true;
        }
        return false;
    }

    public takeDamage(damage: number) {
        if (super.takeDamage(damage)) {
            if (this.isDeleted()) {
                this.megamanBattle?.playDeletedAnimation();
            } else {
                this.megamanBattle?.playHurtAnimation();
            }
            return true;
        }
        return false;
    }
}
