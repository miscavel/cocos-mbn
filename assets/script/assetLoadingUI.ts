import { _decorator, Component, Node, RichText } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('AssetLoadingUI')
export class AssetLoadingUI extends Component {
    @property(RichText)
    public percentLoadText?: RichText;

    @property(RichText)
    public urlLoadText?: RichText;

    public updateText(progress: number, key?: string) {
        const { percentLoadText, urlLoadText } = this;
        const progressPercent = Math.floor(progress * 100);

        if (percentLoadText) {
            percentLoadText.string = `${progressPercent}%`;
        }
        
        if (urlLoadText) {
            switch(progressPercent) {
                case 100: {
                    urlLoadText.string = 'CLICK TO ENTER';
                    break;
                }

                case 0: {
                    urlLoadText.string = 'LOADING...';
                    break;
                }

                default: {
                    urlLoadText.string = `LOADED ${key}`;
                    break;
                }
            }
        }
    }
}
