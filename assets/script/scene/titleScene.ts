
import { _decorator, Component, director } from 'cc';
import { TitleControl } from '../control/titleControl';
import { SCENE_KEY } from '../enum/scene';
import { TITLE_CONTROL_EVENT } from '../enum/titleControl';
import { TRANSITION_SCREEN_EVENT } from '../enum/transitionScreen';
import { Title } from '../object/title';
import { TitleMenu } from '../object/titleMenu';
import { TransitionScreen } from '../sprite/transitionScreen';
const { ccclass, property } = _decorator;

@ccclass('TitleScene')
export class TitleScene extends Component {
    @property(TransitionScreen)
    public readonly transitionScreen?: TransitionScreen | null;

    @property(Title)
    public readonly title?: Title;

    @property(TitleMenu)
    public readonly titleMenu?: TitleMenu;

    @property(TitleControl)
    public readonly titleControl?: TitleControl;

    start () {
        this.title?.play();

        this.transitionScreen?.node.once(TRANSITION_SCREEN_EVENT.FADE_OUT_COMPLETE, () => {
            this.onTransitionEnd();
        });
        this.transitionScreen?.fadeOut(2);
    }

    private onTransitionEnd() {
        this.titleMenu?.progress();

        this.titleControl?.registerTouchEvent();
        this.titleControl?.node.on(TITLE_CONTROL_EVENT.TOUCH_END, () => {
            this.titleMenu?.progress();
            if (this.titleMenu?.isEnteringGame()) {
                this.onComplete();
            }
        });
    }

    private onComplete() {
        this.titleControl?.unregisterTouchEvent();
        this.title?.fadeOut(1);
        this.goToBattleScene();
    }

    private goToTransmissionScene() {
        this.transitionScreen?.fadeIn(1);
        this.transitionScreen?.node.once(TRANSITION_SCREEN_EVENT.FADE_IN_COMPLETE, () => {
            director.loadScene(SCENE_KEY.TRANSMISSION);
        });
    }

    private goToBattleScene() {
        this.transitionScreen?.fadeIn(1);
        this.transitionScreen?.node.once(TRANSITION_SCREEN_EVENT.FADE_IN_COMPLETE, () => {
            director.loadScene(SCENE_KEY.BATTLE);
        });
    }
}