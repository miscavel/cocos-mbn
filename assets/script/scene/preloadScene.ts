import { _decorator, Component, Node, director } from 'cc';
import { ASSET_LOADER_EVENT } from '../enum/assetLoader';
import { SCENE_KEY } from '../enum/scene';
import { AssetLoader } from '../assetLoader';
import { AssetLoadingUI } from '../assetLoadingUI';
import { TransitionScreen } from '../sprite/transitionScreen';
import { TRANSITION_SCREEN_EVENT } from '../enum/transitionScreen';
import { PreloadControl } from '../control/preloadControl';
import { PRELOAD_CONTROL_EVENT } from '../enum/preloadControl';
import { SilentTrack } from '../audio/misc/silentTrack';
const { ccclass, property } = _decorator;

@ccclass('PreloadScene')
export class PreloadScene extends Component {
    @property(AssetLoader)
    public readonly assetLoader?: AssetLoader;

    @property(AssetLoadingUI)
    public readonly assetLoadingUI?: AssetLoadingUI;

    @property(TransitionScreen)
    public readonly transitionScreen?: TransitionScreen;

    @property(PreloadControl)
    public readonly preloadControl?: PreloadControl;

    @property(SilentTrack)
    public readonly silentTrack?: SilentTrack;

    start () {
        this.startAssetsLoad();
    }

    private startAssetsLoad() {
        const { assetLoader, assetLoadingUI } = this;

        if (!assetLoader || !assetLoadingUI) return;

        assetLoader.node.on(ASSET_LOADER_EVENT.START, (progress: number) => {
            assetLoadingUI.updateText(progress);
        });

        assetLoader.node.on(ASSET_LOADER_EVENT.ASSET_LOAD_SUCCESS, (progress: number, key:string) => {
            assetLoadingUI.updateText(progress, key);
        });

        assetLoader.node.on(ASSET_LOADER_EVENT.COMPLETE, (progress: number) => {
            assetLoadingUI.updateText(progress);
            this.onComplete();
        });

        assetLoader.startAssetsLoad();
    }

    private onComplete() {
        // To enable sound by forcing user interaction (web)
        this.silentTrack?.play();

        this.preloadControl?.registerTouchEvent();
        this.preloadControl?.node.once(PRELOAD_CONTROL_EVENT.TOUCH_END, () => {
            this.goToTitleScene();
        });
    }

    private goToTitleScene() {
        this.transitionScreen?.fadeIn(1.5);
        this.transitionScreen?.node.once(TRANSITION_SCREEN_EVENT.FADE_IN_COMPLETE, () => {
            director.loadScene(SCENE_KEY.TITLE);
        });
    }
}
